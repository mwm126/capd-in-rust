use crate::crc16;
use crate::decryptAES128ECB;
use crate::decryptAES256CBC;
use crate::in_addr;
use crate::inet_ntop;
use crate::inet_pton;
use crate::lookUpSerial;
use crate::packet_t;
use crate::plain_t;
use crate::sockaddr_in;
use crate::to_u8;
use crate::utility::arrayToHexString;
use crate::utility::fatal;
use crate::utility::Logger;
use crate::OTP_t;
use crate::SHA256Hash;
use crate::SHA1HMAC;
use crate::SHA256;

/* *****************************************************/
/*    Core process that handles logging and crypto    */
/* *****************************************************/
#[no_mangle]
pub unsafe fn coreProcess(
    configuration: crate::config::Configuration,
    coreToNetSocket: libc::c_int,
    coreToAuthSocket: libc::c_int,
) {
    let mut netBuffer: [libc::c_char; 1024] = [0; 1024];
    let mut serial: libc::c_int;
    let mut destPort: libc::c_int = 0;
    let mut timeout: libc::c_int;
    let mut username: [libc::c_char; 32] = [0; 32];
    let mut destSystem: [libc::c_char; 32] = [0; 32];
    let mut remoteAddr: [u8; 16] = [0; 16];
    let mut serverAddr: [u8; 16] = [0; 16];
    let mut AES128Key: [u8; 16] = [0; 16];
    let mut AES256Key: [u8; 32] = [0; 32];
    let mut HMACKey: [u8; 20] = [0; 20];
    let packet: *mut packet_t = netBuffer.as_mut_ptr() as *mut packet_t;
    let lf = libc::fopen(
        std::ffi::CString::new(configuration.LOG_FILE.clone())
            .unwrap()
            .into_raw(),
        b"a\x00" as *const u8 as *const libc::c_char,
    );
    if lf.is_null() {
        fatal(&format!(
            "SERVER HALT - Error opening log file {}",
            configuration.LOG_FILE
        ));
    }
    let cf = libc::fopen(
        std::ffi::CString::new(configuration.COUNTERFILE.clone())
            .unwrap()
            .into_raw(),
        b"r+\x00" as *const u8 as *const libc::c_char,
    );
    if cf.is_null() {
        fatal(&format!(
            "SERVER HALT - Error opening counter file {}",
            configuration.COUNTERFILE
        ));
    }
    let pf = libc::fopen(
        std::ffi::CString::new(configuration.PASSWD_FILE.clone())
            .unwrap()
            .into_raw(),
        b"r\x00" as *const u8 as *const libc::c_char,
    );
    if pf.is_null() {
        fatal(&format!(
            "SERVER HALT - Error opening password file {}",
            configuration.PASSWD_FILE
        ));
    }
    /* Enter jail and drop privilege */
    let processJailPath =
        std::ffi::CString::new(format!("{}/core", configuration.JAIL_PATH)).unwrap();
    libc::mkdir(processJailPath.as_ptr(), 64);
    libc::chown(processJailPath.as_ptr(), 0, 0);
    libc::chmod(processJailPath.as_ptr(), 64);
    libc::chdir(processJailPath.as_ptr());
    libc::chroot(processJailPath.as_ptr());
    libc::setgid(configuration.gid as u32);
    libc::setuid(configuration.uid as u32);
    if libc::getgid() != configuration.gid as libc::c_uint
        || libc::getuid() != configuration.uid as libc::c_uint
    {
        fatal("SERVER HALT - Privilege not dropped in Core process.");
    }

    let logger = Logger::new(configuration.clone());
    /* Dump initialization info into log file */
    logger.logOutput(lf, 0, "Starting CAPD");
    logger.logOutput(
        lf,
        0,
        &format!("  Password File     : {}", configuration.PASSWD_FILE),
    );
    logger.logOutput(
        lf,
        0,
        &format!("  Counter File      : {}", configuration.COUNTERFILE),
    );
    logger.logOutput(
        lf,
        0,
        &format!("  Log File          : {}", configuration.LOG_FILE),
    );
    logger.logOutput(
        lf,
        0,
        &format!("  Jail Directory    : {}", configuration.JAIL_PATH),
    );
    logger.logOutput(
        lf,
        0,
        &format!("  Packet DeltaT     : {}", configuration.deltaT),
    );
    logger.logOutput(
        lf,
        0,
        &format!("  Login Timeout     : {}", configuration.initTimeout),
    );
    logger.logOutput(
        lf,
        0,
        &format!("  Spoof Timeout     : {}", configuration.spoofTimeout),
    );
    logger.logOutput(
        lf,
        0,
        &format!(
            "  Firewall Script   : {:?}",
            configuration.OPEN_SSH_PATH.as_ptr()
        ),
    );
    logger.logOutput(
        lf,
        0,
        &format!("  Unprivileged User : {:?}", configuration.user.as_ptr()),
    );
    logger.logOutput(
        lf,
        0,
        &format!("  UDP Port          : {}", configuration.port),
    );
    logger.logOutput(
        lf,
        0,
        &format!(
            "  Interface Address : {:?}",
            configuration.SERVER_ADDRESS.as_ptr()
        ),
    );
    logger.logOutput(
        lf,
        0,
        &format!("  Verbosity Level   : {}", configuration.verbosity),
    );
    logger.logOutput(
        lf,
        0,
        &format!("  CAPD Drop Priv uid: {}", configuration.uid),
    );
    logger.logOutput(
        lf,
        0,
        &format!("  CAPD Drop Priv gid: {}", configuration.gid),
    );
    /* Parse server address */
    let mut sa: sockaddr_in = sockaddr_in {
        sin_family: 0,
        sin_port: 0,
        sin_addr: in_addr { s_addr: 0 },
        sin_zero: [0; 8],
    };
    inet_pton(
        2,
        std::ffi::CString::new(configuration.SERVER_ADDRESS.clone())
            .unwrap()
            .as_ptr(),
        &mut sa.sin_addr as *mut in_addr as *mut libc::c_void,
    );
    libc::memcpy(
        serverAddr.as_ptr().offset(12isize) as *mut libc::c_void,
        &mut sa.sin_addr as *mut in_addr as *const libc::c_void,
        4,
    );
    loop {
        /* Cryptographically parse and verify packet */
        if libc::read(
            coreToNetSocket,
            remoteAddr.as_ptr() as *mut libc::c_void,
            ::std::mem::size_of::<[u8; 16]>(),
        ) as libc::c_ulong
            != ::std::mem::size_of::<[u8; 16]>() as libc::c_ulong
            || libc::read(
                coreToNetSocket,
                netBuffer.as_ptr() as *mut libc::c_void,
                1024,
            ) as libc::c_ulong
                != ::std::mem::size_of::<packet_t>() as libc::c_ulong
        {
            logger.logOutput(
                lf,
                0,
                "CRITICAL FAULT - Wrong-sized message received from Net Process!",
            );
        } else {
            serial = (*packet).serial;
            /* Look up user information */
            if 0 == lookUpSerial(
                pf,
                serial,
                username.as_mut_ptr(),
                destSystem.as_mut_ptr(),
                &mut destPort,
                AES128Key.as_mut_ptr(),
                HMACKey.as_mut_ptr(),
            ) {
                logger.logOutput(lf, 1, &format!("Failed serial lookup: {}", &serial));
            } else {
                /* Construct SHA1-HMAC response and AES256 Key */
                let mut response: [u8; 20] = [0; 20];
                let mut tmp: [u8; 100] = [0; 100];
                SHA1HMAC(
                    (*packet).challenge.as_mut_ptr(),
                    32,
                    HMACKey.as_mut_ptr(),
                    20,
                    response.as_mut_ptr(),
                );
                libc::memcpy(
                    tmp.as_mut_ptr() as *mut libc::c_void,
                    response.as_mut_ptr() as *const libc::c_void,
                    20,
                );
                libc::memcpy(
                    tmp.as_mut_ptr().offset(20) as *mut libc::c_void,
                    (*packet).challenge.as_mut_ptr() as *const libc::c_void,
                    32,
                );
                SHA256Hash(tmp.as_mut_ptr(), 20 + 32, AES256Key.as_mut_ptr());

                let mut plainBuffer: [u8; 160] = [0; 160];

                /* Decrypt encBlock */
                decryptAES256CBC(
                    (*packet).encBlock.as_mut_ptr(),
                    ::std::mem::size_of::<plain_t>() as libc::c_ulong as libc::c_int,
                    AES256Key.as_mut_ptr(),
                    (*packet).IV.as_mut_ptr(),
                    plainBuffer.as_mut_ptr(),
                );
                /* Verify plaintext chksum */
                let mut digest: [u8; 32] = [0; 32];
                SHA256Hash(
                    plainBuffer.as_mut_ptr(),
                    (::std::mem::size_of::<plain_t>() as libc::c_ulong)
                        .wrapping_sub(32i32 as libc::c_ulong) as libc::c_int,
                    digest.as_mut_ptr(),
                );

                let mut PLAIN: plain_t = plain_t {
                    authAddr: [0; 16],
                    connAddr: [0; 16],
                    serverAddr: [0; 16],
                    OTP: [0; 16],
                    username: [0; 32],
                    entropy: [0; 32],
                    chksum: [0; 32],
                };

                if libc::memcmp(
                    digest.as_mut_ptr() as *mut libc::c_void,
                    PLAIN.chksum.as_mut_ptr() as *mut libc::c_void,
                    32,
                ) != 0
                {
                    logger.logOutput(
                        lf,
                        1,
                        &format!("Decrypted block failed chksum.  Serial: {}", serial),
                    );
                } else if libc::memcmp(
                    PLAIN.serverAddr.as_mut_ptr() as *const libc::c_void,
                    serverAddr.as_mut_ptr() as *const libc::c_void,
                    16,
                ) != 0
                {
                    logger.logOutput(
                        lf,
                        1,
                        &format!("Bad server address in decrypted block.  Serial: {}", serial),
                    );
                    let serverAddrHex = arrayToHexString(&serverAddr);
                    logger.logOutput(lf, 2, &format!("     Packet Value   : {}", serverAddrHex));
                    let plainServerAddrHex = arrayToHexString(&PLAIN.serverAddr);
                    logger.logOutput(
                        lf,
                        2,
                        &format!("     Decrypted Value: {}", plainServerAddrHex),
                    );
                } else if libc::memcmp(
                    PLAIN.authAddr.as_mut_ptr() as *const libc::c_void,
                    remoteAddr.as_mut_ptr() as *const libc::c_void,
                    16,
                ) != 0
                {
                    logger.logOutput(
                        lf,
                        1,
                        &format!("Bad client address in decrypted block.  Serial: {}", serial),
                    );
                    let clientAddrHex = arrayToHexString(&remoteAddr);
                    logger.logOutput(lf, 2, &format!("     Packet Value   : {}", clientAddrHex));
                    let authAddrHex = arrayToHexString(&PLAIN.authAddr);
                    logger.logOutput(lf, 2, &format!("     Decrypted Value: {}", authAddrHex));
                } else if libc::strcmp(
                    PLAIN.username.as_mut_ptr(),
                    username.as_mut_ptr() as *const i8,
                ) != 0
                {
                    logger.logOutput(
                        lf,
                        1,
                        &format!("Username verification failed: {:?}", PLAIN.username),
                    );
                } else {
                    /* Verify challenge construction */
                    let mut tmp_0: [u8; 100] = [0; 100];
                    let mut digest_0: [u8; 32] = [0; 32];
                    libc::memcpy(
                        tmp_0.as_mut_ptr() as *mut libc::c_void,
                        b"SHA1-HMACChallenge\x00" as *const u8 as *const libc::c_char
                            as *const libc::c_void,
                        18,
                    );
                    libc::memcpy(
                        tmp_0.as_mut_ptr().offset(18isize) as *mut libc::c_void,
                        PLAIN.OTP.as_mut_ptr() as *const libc::c_void,
                        16,
                    );
                    libc::memcpy(
                        tmp_0.as_mut_ptr().offset(18isize).offset(16isize) as *mut libc::c_void,
                        PLAIN.entropy.as_mut_ptr() as *const libc::c_void,
                        32,
                    );
                    SHA256(tmp_0.as_mut_ptr(), 18 + 16 + 32, digest_0.as_mut_ptr());
                    if libc::memcmp(
                        (*packet).challenge.as_mut_ptr() as *const libc::c_void,
                        digest_0.as_mut_ptr() as *const libc::c_void,
                        32,
                    ) != 0
                    {
                        logger.logOutput(
                            lf,
                            1,
                            &format!(
                                "Challenge construction verification failed.  Serial: {}",
                                serial
                            ),
                        );
                    } else {
                        /* Verify IV construction */
                        let mut tmp_1: [u8; 100] = [0; 100];
                        let mut digest_1: [u8; 32] = [0; 32];
                        libc::memcpy(
                            tmp_1.as_mut_ptr() as *mut libc::c_void,
                            PLAIN.entropy.as_mut_ptr() as *const libc::c_void,
                            32,
                        );
                        libc::memcpy(
                            tmp_1.as_mut_ptr().offset(32isize) as *mut libc::c_void,
                            PLAIN.OTP.as_mut_ptr() as *const libc::c_void,
                            16,
                        );
                        SHA256Hash(tmp_1.as_mut_ptr(), 32i32 + 16i32, digest_1.as_mut_ptr());
                        if libc::memcmp(
                            (*packet).IV.as_mut_ptr() as *const libc::c_void,
                            digest_1.as_mut_ptr() as *const libc::c_void,
                            16,
                        ) != 0
                        {
                            logger.logOutput(
                                lf,
                                1,
                                &format!(
                                    "IV construction verification failed.  Serial: {}",
                                    serial
                                ),
                            );
                        } else {
                            /* Verify OTP */
                            let mut OTPBuffer: [u8; 16] = [0; 16];
                            let mut tmp_2: [u8; 100] = [0; 100];
                            let mut digest_2: [u8; 32] = [0; 32];
                            let OTP: *mut OTP_t = OTPBuffer.as_mut_ptr() as *mut OTP_t;
                            decryptAES128ECB(
                                PLAIN.OTP.as_mut_ptr(),
                                ::std::mem::size_of::<OTP_t>() as libc::c_ulong as libc::c_int,
                                AES128Key.as_mut_ptr(),
                                OTPBuffer.as_mut_ptr(),
                            );
                            /* Verify decrypted CRC */
                            if 0xf0b8 != crc16(OTPBuffer.as_mut_ptr(), 16) {
                                logger.logOutput(
                                    lf,
                                    1,
                                    &format!("OTP CRC verification failed.  Serial: {}", serial),
                                );
                            } else {
                                /* Check OTP Challenge */
                                crate::libc::memcpy(
                                    tmp_2.as_mut_ptr() as *mut libc::c_void,
                                    b"yubicoChal\x00" as *const u8 as *const libc::c_char
                                        as *const libc::c_void,
                                    10,
                                );
                                crate::libc::memcpy(
                                    tmp_2.as_mut_ptr().offset(10isize) as *mut libc::c_void,
                                    PLAIN.entropy.as_mut_ptr() as *const libc::c_void,
                                    32,
                                );
                                SHA256(tmp_2.as_mut_ptr(), 10 + 32, digest_2.as_mut_ptr());
                                if libc::memcmp(
                                    digest_2.as_mut_ptr() as *const libc::c_void,
                                    (*OTP).OTPChallenge.as_mut_ptr() as *const libc::c_void,
                                    6,
                                ) != 0
                                {
                                    logger.logOutput(
                                        lf,
                                        1,
                                        &format!(
                                            "OTP Challenge verification failed.  Serial: {}",
                                            serial
                                        ),
                                    );
                                } else {
                                    let counter = (*OTP).insertCounter as libc::c_int * 256
                                        + (*OTP).sessionCounter as libc::c_int;
                                    /* Check counter */
                                    if counter <= counterfile::searchCounterFile(cf, serial) {
                                        logger.logOutput(
                                            lf,
                                            1,
                                            &format!(
                                                "OTP Counter verification failed.  Serial: {}",
                                                serial
                                            ),
                                        );
                                    } else {
                                        counterfile::updateCounterFileEntry(cf, serial, counter);
                                        logger.logOutput(
                                            lf,
                                            1,
                                            &format!(
                                                "Accepted connection: {}, {}, {}",
                                                std::str::from_utf8(&to_u8(&username)).unwrap(),
                                                serial,
                                                counter,
                                            ),
                                        );
                                        /* Setup timeout */
                                        if libc::memcmp(
                                            PLAIN.connAddr.as_mut_ptr() as *const libc::c_void,
                                            PLAIN.authAddr.as_mut_ptr() as *const libc::c_void,
                                            16,
                                        ) == 0
                                        {
                                            timeout = configuration.initTimeout as libc::c_int
                                        } else {
                                            timeout = configuration.spoofTimeout as libc::c_int
                                        }
                                        /* Translate connIPbinary to IP as text */
                                        let mut connAddrTxt: [libc::c_char; 46] = [0; 46];
                                        let mut allZeros: [u8; 12] = [0; 12];
                                        /* IPV4 */
                                        if libc::memcmp(
                                            PLAIN.connAddr.as_mut_ptr() as *const libc::c_void,
                                            allZeros.as_mut_ptr() as *const libc::c_void,
                                            12,
                                        ) == 0
                                        {
                                            let mut ipv4: [u8; 4] = [0; 4];
                                            libc::memcpy(
                                                ipv4.as_mut_ptr() as *mut libc::c_void,
                                                PLAIN.connAddr.as_mut_ptr().offset(12isize)
                                                    as *const libc::c_void,
                                                4,
                                            );
                                            inet_ntop(
                                                2,
                                                ipv4.as_mut_ptr() as *const libc::c_void,
                                                connAddrTxt.as_mut_ptr(),
                                                16i32 as libc::socklen_t,
                                            );
                                        } else {
                                            /* IPV6 */
                                            inet_ntop(
                                                10i32,
                                                PLAIN.connAddr.as_mut_ptr() as *const libc::c_void,
                                                connAddrTxt.as_mut_ptr(),
                                                46i32 as libc::socklen_t,
                                            );
                                        }
                                        /* Authorize connection */
                                        libc::write(
                                            coreToAuthSocket,
                                            connAddrTxt.as_mut_ptr() as *const libc::c_void,
                                            46,
                                        );
                                        libc::write(
                                            coreToAuthSocket,
                                            destSystem.as_mut_ptr() as *const libc::c_void,
                                            ::std::mem::size_of::<[libc::c_char; 32]>(),
                                        );
                                        libc::write(
                                            coreToAuthSocket,
                                            &mut destPort as *mut libc::c_int
                                                as *const libc::c_void,
                                            ::std::mem::size_of::<libc::c_int>(),
                                        );
                                        libc::write(
                                            coreToAuthSocket,
                                            &mut timeout as *mut libc::c_int as *const libc::c_void,
                                            ::std::mem::size_of::<libc::c_int>(),
                                        );
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

mod counterfile {

    #[no_mangle]
    pub unsafe fn searchCounterFile(f: *mut libc::FILE, serial: libc::c_int) -> libc::c_int {
        let mut s: libc::c_int = 0;
        let mut c: libc::c_int = 0;
        let mut r: libc::c_int = -1;
        libc::rewind(f);
        while libc::fscanf(
            f,
            b"%9d %9d\n\x00" as *const u8 as *const libc::c_char,
            &mut s as *mut libc::c_int,
            &mut c as *mut libc::c_int,
        ) != -1
        {
            if s != serial {
                continue;
            }
            r = c;
            break;
        }
        r
    }
    #[no_mangle]
    pub unsafe fn updateCounterFileEntry(
        counterfile: *mut libc::FILE,
        serial: libc::c_int,
        counter: libc::c_int,
    ) {
        let mut i: libc::c_int;
        let mut n: libc::c_int = 0;
        let mut s: libc::c_int = 0;
        let mut c: libc::c_int = 0;
        let mut found: libc::c_int = 0;
        let mut serialList: [libc::c_int; 2000] = [0; 2000];
        let mut counterList: [libc::c_int; 2000] = [0; 2000];
        libc::rewind(counterfile);
        while libc::fscanf(
            counterfile,
            b"%9d %9d\n\x00" as *const u8 as *const libc::c_char,
            &mut s as *mut libc::c_int,
            &mut c as *mut libc::c_int,
        ) != -1
            && n < 2000i32
        {
            serialList[n as usize] = s;
            if s != serial {
                counterList[n as usize] = c
            } else {
                counterList[n as usize] = counter;
                found = 1
            }
            n += 1
        }
        libc::rewind(counterfile);
        libc::ftruncate(libc::fileno(counterfile), 0);
        i = 0;
        while i < n {
            libc::fprintf(
                counterfile,
                b"%d %d\n\x00" as *const u8 as *const libc::c_char,
                serialList[i as usize],
                counterList[i as usize],
            );
            i += 1
        }
        if 0 == found {
            libc::printf(b"Writing NEW serial/counter\n\x00" as *const u8 as *const libc::c_char);
            libc::fprintf(
                counterfile,
                b"%d %d\n\x00" as *const u8 as *const libc::c_char,
                serial,
                counter,
            );
        }
        libc::fflush(counterfile);
    }
}
