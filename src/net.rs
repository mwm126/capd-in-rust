use crate::htonl;
use crate::htons;
use crate::in_addr;
use crate::initMaps;
use crate::packet_t;
use crate::replaySet;
use crate::replayTest;
use crate::sockaddr_in;
use crate::socket;
use crate::SHA256Hash;

/* ************************************************************/
/*   Opens UDP socket and does basic processing of packets   */
/* ************************************************************/
#[no_mangle]
pub unsafe fn netProcess(
    configuration: crate::config::Configuration,
    netToCoreSocket: libc::c_int,
) {
    let mut siLocal: sockaddr_in = sockaddr_in {
        sin_family: 0,
        sin_port: 0,
        sin_addr: in_addr { s_addr: 0 },
        sin_zero: [0; 8],
    };
    let mut siRemote: sockaddr_in = sockaddr_in {
        sin_family: 0,
        sin_port: 0,
        sin_addr: in_addr { s_addr: 0 },
        sin_zero: [0; 8],
    };
    let mut udpBuffer: [u8; 1024] = [0; 1024];
    let packet_0: *mut packet_t = udpBuffer.as_mut_ptr() as *mut packet_t;
    let mut tmp: [u8; 280] = [0; 280];
    let mut MAC: [u8; 32] = [0; 32];
    let key: *mut u32 = (*packet_0).MAC.as_mut_ptr() as *mut u32;
    /* Clear out address structs */
    libc::memset(
        &mut siLocal as *mut sockaddr_in as *mut libc::c_void,
        0i32,
        ::std::mem::size_of::<sockaddr_in>(),
    );
    libc::memset(
        &mut siRemote as *mut sockaddr_in as *mut libc::c_void,
        0i32,
        ::std::mem::size_of::<sockaddr_in>(),
    );
    /* De-obfuscate Shared Secrets */
    derivefixedkeys::deriveFixedKeys();
    /* Setup Replay Bitmaps */
    initMaps();
    /* Open UDP Port */
    let udpSocket = socket(
        2i32,
        libc::SOCK_DGRAM as libc::c_int,
        libc::IPPROTO_UDP as libc::c_int,
    );
    if udpSocket < 0i32 {
        crate::utility::fatal("SERVER HALT - UDP Socket creation failed.");
    }
    siLocal.sin_family = 2i32 as libc::sa_family_t;
    siLocal.sin_port = htons(configuration.port as u16);
    siLocal.sin_addr.s_addr = htonl(0);
    if libc::bind(
        udpSocket,
        &mut siLocal as *mut sockaddr_in as *mut libc::sockaddr,
        ::std::mem::size_of::<sockaddr_in>() as libc::c_ulong as libc::socklen_t,
    ) < 0i32
    {
        crate::utility::fatal("SERVER HALT - Bind of UDP Socket failed.\x00");
    }
    /* Enter jail and drop privilege */
    let processJailPath =
        std::ffi::CString::new(format!("{}/net", configuration.JAIL_PATH)).unwrap();
    libc::mkdir(processJailPath.as_ptr(), 64);
    libc::chown(processJailPath.as_ptr(), 0, 0);
    libc::chmod(processJailPath.as_ptr(), 64);
    libc::chdir(processJailPath.as_ptr());
    libc::chroot(processJailPath.as_ptr());
    libc::setgid(configuration.gid as u32);
    libc::setuid(configuration.uid as u32);
    if libc::getgid() != configuration.gid as libc::c_uint
        || libc::getuid() != configuration.uid as libc::c_uint
    {
        crate::utility::fatal("SERVER HALT - Privilege not dropped in Net process.");
    }
    libc::memset(udpBuffer.as_mut_ptr() as *mut libc::c_void, 0i32, 1024);
    /* Server loop */
    loop {
        let mut lenRemote =
            ::std::mem::size_of::<sockaddr_in>() as libc::c_ulong as libc::socklen_t;
        let len = libc::recvfrom(
            udpSocket,
            udpBuffer.as_mut_ptr() as *mut libc::c_void,
            1024,
            0i32,
            &mut siRemote as *mut sockaddr_in as *mut libc::sockaddr,
            &mut lenRemote,
        ) as libc::c_int;
        /* Basic tests */
        if len as libc::c_ulong != ::std::mem::size_of::<packet_t>() as libc::c_ulong {
            continue;
        }
        let localTime = crate::utility::currentTime() as libc::c_int;
        if libc::abs((*packet_0).timestamp - localTime) as libc::c_uint > configuration.deltaT {
            continue;
        }
        /* replay test */
        if 0 != replayTest(configuration.deltaT, *key.offset(0isize), localTime) {
            continue;
        }
        /* SHA256 MAC test */
        libc::memcpy(
            tmp.as_mut_ptr() as *mut libc::c_void,
            derivefixedkeys::fixedHeaderKey.as_mut_ptr() as *const libc::c_void,
            32,
        );
        libc::memcpy(
            tmp.as_mut_ptr().offset(32isize) as *mut libc::c_void,
            udpBuffer.as_mut_ptr() as *const libc::c_void,
            (::std::mem::size_of::<packet_t>()).wrapping_sub(32),
        );
        libc::memcpy(
            tmp.as_mut_ptr().offset(32isize).offset(
                (::std::mem::size_of::<packet_t>() as libc::c_ulong)
                    .wrapping_sub(32i32 as libc::c_ulong) as isize,
            ) as *mut libc::c_void,
            derivefixedkeys::fixedFooterKey.as_mut_ptr() as *const libc::c_void,
            32,
        );
        SHA256Hash(
            tmp.as_mut_ptr(),
            (32i32 as libc::c_ulong)
                .wrapping_add(
                    (::std::mem::size_of::<packet_t>() as libc::c_ulong)
                        .wrapping_sub(32i32 as libc::c_ulong),
                )
                .wrapping_add(32i32 as libc::c_ulong) as libc::c_int,
            MAC.as_mut_ptr(),
        );
        if libc::memcmp(
            MAC.as_mut_ptr() as *const libc::c_void,
            (*packet_0).MAC.as_mut_ptr() as *const libc::c_void,
            32,
        ) != 0i32
        {
            continue;
        }
        /* Insert valid MAC into replay database */
        replaySet(&configuration, *key.offset(0isize), localTime);
        /* Ship out packet to core process */
        let mut remoteAddr: [u8; 16] = [0i32 as u8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        libc::memcpy(
            remoteAddr.as_mut_ptr().offset(12isize) as *mut libc::c_void,
            &mut (*(&mut siRemote as *mut sockaddr_in)).sin_addr as *mut in_addr
                as *const libc::c_void,
            4,
        );
        libc::write(
            netToCoreSocket,
            remoteAddr.as_mut_ptr() as *const libc::c_void,
            ::std::mem::size_of::<[u8; 16]>(),
        );
        libc::write(
            netToCoreSocket,
            udpBuffer.as_mut_ptr() as *const libc::c_void,
            ::std::mem::size_of::<packet_t>(),
        );
    }
}

mod derivefixedkeys {

    /* ***********************************************************/
    /*    Derives shared keys from seed and simple algorithm    */
    /* ***********************************************************/

    #[no_mangle]
    pub unsafe fn deriveFixedKeys() {
        let mut generator: libc::c_int = 0x55i32;
        for i in 0..32 {
            let byte: libc::c_int = seedHeader[i as usize] as libc::c_int;
            generator = (generator * byte + 0x12i32) % 256i32;
            fixedHeaderKey[i as usize] = (generator ^ byte) as u8;
        }
        generator = 0x18i32;
        for i in 0..32 {
            let byte_0: libc::c_int = seedFooter[i as usize] as libc::c_int;
            generator = (generator * byte_0 + 0xe2i32) % 256i32;
            fixedFooterKey[i as usize] = (generator ^ byte_0) as u8;
        }
        for i in 0..32 {
            seedHeader[i as usize] = 0xffi32 as u8;
        }
        for i in 0..32 {
            seedFooter[i as usize] = 0xffi32 as u8;
        }
    }

    #[no_mangle]
    static mut seedHeader: [u8; 32] = [
        0xefi32 as u8,
        0xa8i32 as u8,
        0xfei32 as u8,
        0x36i32 as u8,
        0xebi32 as u8,
        0x80i32 as u8,
        0x2i32 as u8,
        0x5ci32 as u8,
        0xfi32 as u8,
        0xfdi32 as u8,
        0x9i32 as u8,
        0x1ai32 as u8,
        0xa9i32 as u8,
        0x1ci32 as u8,
        0x50i32 as u8,
        0xf8i32 as u8,
        0x3ei32 as u8,
        0xebi32 as u8,
        0x52i32 as u8,
        0x74i32 as u8,
        0x9ci32 as u8,
        0x56i32 as u8,
        0xa4i32 as u8,
        0x44i32 as u8,
        0x7bi32 as u8,
        0x31i32 as u8,
        0x6ci32 as u8,
        0x1ai32 as u8,
        0xe5i32 as u8,
        0xbci32 as u8,
        0xf7i32 as u8,
        0x5di32 as u8,
    ];
    #[no_mangle]
    static mut seedFooter: [u8; 32] = [
        0x42i32 as u8,
        0x3ci32 as u8,
        0x5i32 as u8,
        0xf2i32 as u8,
        0xc4i32 as u8,
        0x9bi32 as u8,
        0x8ci32 as u8,
        0x3ei32 as u8,
        0x79i32 as u8,
        0x16i32 as u8,
        0xbai32 as u8,
        0xd2i32 as u8,
        0x54i32 as u8,
        0xd7i32 as u8,
        0x92i32 as u8,
        0x48i32 as u8,
        0xc2i32 as u8,
        0x55i32 as u8,
        0xbai32 as u8,
        0x8ci32 as u8,
        0xe2i32 as u8,
        0xe5i32 as u8,
        0xe5i32 as u8,
        0xd2i32 as u8,
        0x1bi32 as u8,
        0x1ai32 as u8,
        0x1ci32 as u8,
        0xbci32 as u8,
        0x49i32 as u8,
        0xabi32 as u8,
        0x28i32 as u8,
        0x18i32 as u8,
    ];
    #[no_mangle]
    pub static mut fixedHeaderKey: [u8; 32] = [0; 32];
    #[no_mangle]
    pub static mut fixedFooterKey: [u8; 32] = [0; 32];

}
