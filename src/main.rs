#![allow(non_snake_case)]

extern crate clap;
use clap::{App, Arg};

use std::net::IpAddr;

pub mod auth;
pub mod core;
pub mod net;
pub mod utility;

fn to_u8(unsigned_array: &[i8]) -> Vec<u8> {
    unsigned_array.iter().map(|&x| x as u8).collect()
}

extern crate libc;
extern "C" {
    #[no_mangle]
    fn htonl(__hostlong: u32) -> u32;
    #[no_mangle]
    fn htons(__hostshort: u16) -> u16;
    #[no_mangle]
    fn inet_pton(
        __af: libc::c_int,
        __cp: *const libc::c_char,
        __buf: *mut libc::c_void,
    ) -> libc::c_int;
    #[no_mangle]
    fn inet_ntop(
        __af: libc::c_int,
        __cp: *const libc::c_void,
        __buf: *mut libc::c_char,
        __len: libc::socklen_t,
    ) -> *const libc::c_char;
}

#[link(name = "crypto")]
extern "C" {
    #[no_mangle]
    fn AES_set_decrypt_key(
        userKey: *const libc::c_uchar,
        bits: libc::c_int,
        key: *mut AesKey,
    ) -> libc::c_int;

    #[no_mangle]
    fn AES_ecb_encrypt(
        in_0: *const libc::c_uchar,
        out: *mut libc::c_uchar,
        key: *const AesKey,
        enc: libc::c_int,
    );
    #[no_mangle]
    fn AES_cbc_encrypt(
        in_0: *const libc::c_uchar,
        out: *mut libc::c_uchar,
        length: libc::size_t,
        key: *const AesKey,
        ivec: *mut libc::c_uchar,
        enc: libc::c_int,
    );
    #[no_mangle]
    fn strftime(
        __s: *mut libc::c_char,
        __maxsize: libc::size_t,
        __format: *const libc::c_char,
        __tp: *const libc::tm,
    ) -> libc::size_t;
    #[no_mangle]
    fn clock_gettime(__clock_id: libc::clockid_t, __tp: *mut libc::timespec) -> libc::c_int;
    #[no_mangle]
    fn EVP_sha1() -> *const libc::c_void;
    #[no_mangle]
    fn HMAC(
        evp_md: *const libc::c_void,
        key: *const libc::c_void,
        key_len: libc::c_int,
        d: *const libc::c_uchar,
        n: libc::size_t,
        md: *mut libc::c_uchar,
        md_len: *mut libc::c_uint,
    ) -> *mut libc::c_uchar;
    #[no_mangle]
    fn SHA256(
        d: *const libc::c_uchar,
        n: libc::size_t,
        md: *mut libc::c_uchar,
    ) -> *mut libc::c_uchar;
    #[no_mangle]
    fn getpwnam(__name: *const libc::c_char) -> *mut libc::passwd;
    #[no_mangle]
    fn socket(__domain: libc::c_int, __type: libc::c_int, __protocol: libc::c_int) -> libc::c_int;
}

#[derive(Copy, Clone)]
#[repr(C)]
struct sockaddr {
    sa_family: libc::c_ushort,
    sa_data: [libc::c_char; 14],
}
#[derive(Copy, Clone)]
#[repr(C)]
struct in_addr {
    s_addr: u32,
}

#[derive(Copy, Clone)]
#[repr(C)]
struct sockaddr_in {
    sin_family: libc::sa_family_t,
    sin_port: u16,
    sin_addr: in_addr,
    sin_zero: [libc::c_uchar; 8],
}
#[derive(Copy, Clone)]
#[repr(C)]
struct aes_key_st {
    rd_key: [libc::c_uint; 60],
    rounds: libc::c_int,
}
type AesKey = aes_key_st;

/* Packet, Payload, and OTP Structures and Buffers */
#[derive(Copy, Clone)]
#[repr(C)]
struct plain_t {
    authAddr: [u8; 16],
    connAddr: [u8; 16],
    serverAddr: [u8; 16],
    OTP: [u8; 16],
    username: [libc::c_char; 32],
    entropy: [u8; 32],
    chksum: [u8; 32],
}
#[derive(Copy, Clone)]
#[repr(C)]
struct packet_t {
    timestamp: libc::c_int,
    serial: libc::c_int,
    IV: [u8; 16],
    challenge: [u8; 32],
    encBlock: [u8; 160],
    MAC: [u8; 32],
}
#[derive(Copy, Clone)]
#[repr(C)]
struct OTP_t {
    OTPChallenge: [u8; 6],
    insertCounter: u16,
    timestamp: [u8; 3],
    sessionCounter: u8,
    random: u16,
    crc: u16,
}

mod config {

    #[derive(Clone)]
    pub struct Configuration {
        pub COUNTERFILE: String,
        /* Global Control Variables */
        pub PASSWD_FILE: String,
        pub LOG_FILE: String,
        pub JAIL_PATH: String,
        pub OPEN_SSH_PATH: String,

        pub SERVER_ADDRESS: String,
        /* Maximum allowed clock skew from client to server */
        #[no_mangle]
        pub deltaT: u32,
        /* Maximum time allowed to open local ssh connection */
        #[no_mangle]
        pub initTimeout: u32,
        /* Maximum time allowed to open spoofed ssh connection */
        #[no_mangle]
        pub spoofTimeout: u32,
        #[no_mangle]
        pub user: String,
        #[no_mangle]
        pub uid: libc::c_int,
        #[no_mangle]
        pub gid: libc::c_int,
        #[no_mangle]
        pub port: u16,
        #[no_mangle]
        pub verbosity: libc::c_int,
    }

}

/* *********************************************************/
/*           Search CAPD Password File Function           */
/* *********************************************************/
#[no_mangle]
unsafe fn lookUpSerial(
    f: *mut libc::FILE,
    serial: libc::c_int,
    username: *mut libc::c_char,
    destSystem: *mut libc::c_char,
    destPort: *mut libc::c_int,
    AES128Key: *mut u8,
    HMACKey: *mut u8,
) -> libc::c_int {
    let mut s: libc::c_int;
    let mut txtPort: [libc::c_char; 9] = [0; 9];
    let mut txtS: [libc::c_char; 21] = [0; 21];
    let mut txtAES: [libc::c_char; 41] = [0; 41];
    let mut txtHMAC: [libc::c_char; 51] = [0; 51];
    libc::lseek(libc::fileno(f), 0, 0);
    while libc::fscanf(
        f,
        b"%32s %32s %8s %20s %40s %50s\n\x00" as *const u8 as *const libc::c_char,
        username,
        destSystem,
        txtPort.as_mut_ptr(),
        txtS.as_mut_ptr(),
        txtAES.as_mut_ptr(),
        txtHMAC.as_mut_ptr(),
    ) != -1
    {
        if *username.offset(0isize) as libc::c_int != 35i32 {
            s = libc::atoi(txtS.as_mut_ptr());
            if serial == s {
                let mut data: libc::c_int = 0;
                *destPort = libc::atoi(txtPort.as_mut_ptr());
                for i in 0..16 {
                    libc::sscanf(
                        txtAES.as_mut_ptr().offset((2i32 * i) as isize) as *const libc::c_char,
                        b"%2x\x00" as *const u8 as *const libc::c_char,
                        &mut data as *mut libc::c_int,
                    );
                    *AES128Key.offset(i as isize) = data as u8;
                }
                for i in 0..20 {
                    libc::sscanf(
                        txtHMAC.as_mut_ptr().offset((2i32 * i) as isize) as *const libc::c_char,
                        b"%2x\x00" as *const u8 as *const libc::c_char,
                        &mut data as *mut libc::c_int,
                    );
                    *HMACKey.offset(i as isize) = data as u8;
                }
                return 1;
            }
        }
    }
    0
}
/* ********************************************/
/*           Endian swap functions           */
/* ********************************************/
#[no_mangle]
unsafe fn SHA1HMAC(
    chal: *mut u8,
    chalLen: libc::c_int,
    key: *mut u8,
    keyLen: libc::c_int,
    resp: *mut u8,
) {
    let mut respLen: u32 = 0;
    HMAC(
        EVP_sha1() as *const libc::c_void,
        key as *mut libc::c_uchar as *const libc::c_void,
        keyLen,
        chal as *mut libc::c_uchar,
        chalLen as usize,
        resp as *mut libc::c_uchar,
        &mut respLen,
    );
}
#[no_mangle]
unsafe fn decryptAES128ECB(
    cryptText: *mut u8,
    cryptLen: libc::c_int,
    key: *mut u8,
    plainText: *mut u8,
) {
    let mut aeskey: AesKey = AesKey {
        rd_key: [0; 60],
        rounds: 0,
    };
    assert_eq!(cryptLen, 128);
    AES_set_decrypt_key(key as *mut libc::c_uchar, 128i32, &mut aeskey);
    AES_ecb_encrypt(
        cryptText as *mut libc::c_uchar,
        plainText as *mut libc::c_uchar,
        &aeskey,
        0,
    );
}
#[no_mangle]
unsafe fn decryptAES256CBC(
    cryptText: *mut u8,
    cryptLen: libc::c_int,
    key: *mut u8,
    IV: *mut u8,
    plainText: *mut u8,
) {
    let mut aeskey: AesKey = AesKey {
        rd_key: [0; 60],
        rounds: 0,
    };
    let mut tmpKey: [u8; 32] = [0; 32];
    let mut tmpIV: [u8; 16] = [0; 16];
    /* Copy key and IV to temp arrays to preserve original information */
    libc::memcpy(
        tmpKey.as_mut_ptr() as *mut libc::c_void,
        key as *const libc::c_void,
        32,
    );
    libc::memcpy(
        tmpIV.as_mut_ptr() as *mut libc::c_void,
        IV as *const libc::c_void,
        16,
    );
    AES_set_decrypt_key(
        tmpKey.as_mut_ptr() as *mut libc::c_uchar,
        256i32,
        &mut aeskey,
    );
    AES_cbc_encrypt(
        cryptText as *mut libc::c_uchar,
        plainText as *mut libc::c_uchar,
        cryptLen as usize,
        &aeskey,
        tmpIV.as_mut_ptr() as *mut libc::c_uchar,
        0,
    );
}
#[no_mangle]
unsafe fn crc16(data: *mut u8, dataLen: libc::c_int) -> u16 {
    let mut crc: u16 = 0xffffi32 as u16;
    for i in 0..dataLen {
        crc = (crc as libc::c_int ^ *data.offset(i as isize) as libc::c_int) as u16;
        for _ in 0..8 {
            let k = crc as libc::c_int & 1;
            crc = (crc as libc::c_int >> 1) as u16;
            if 0 != k {
                crc = (crc as libc::c_int ^ 0x8408i32) as u16
            }
        }
    }
    crc
}
/* **********************************************/
/*     Cryptography Functions and Wrappers     */
/* **********************************************/
#[no_mangle]
unsafe fn SHA256Hash(data: *mut u8, dataLen: libc::c_int, digest: *mut u8) {
    SHA256(
        data as *mut libc::c_uchar,
        dataLen as usize,
        digest as *mut libc::c_uchar,
    );
}

#[no_mangle]
unsafe fn replayTest(deltaT: u32, key: u32, time_0: libc::c_int) -> libc::c_int {
    let modKey: libc::c_int =
        key.wrapping_rem((8 * (4 * 1024 * 1024)) as libc::c_uint) as libc::c_int;
    let window: libc::c_int = (time_0 as libc::c_uint).wrapping_div(deltaT) as libc::c_int;
    for i in 0..3 {
        if mapWindow[i as usize] == window - 1 && 0 != bitTest(modKey as u32, map[i as usize]) {
            return 1;
        }
        if mapWindow[i as usize] == window && 0 != bitTest(modKey as u32, map[i as usize]) {
            return 1;
        }
        if mapWindow[i as usize] == window + 1 && 0 != bitTest(modKey as u32, map[i as usize]) {
            return 1;
        }
    }
    0
}

#[no_mangle]
static mut map: [*mut u8; 3] = [
    0 as *const u8 as *mut u8,
    0 as *const u8 as *mut u8,
    0 as *const u8 as *mut u8,
];

#[no_mangle]
static mut mapWindow: [libc::c_int; 3] = [0, 0, 0];
#[no_mangle]
unsafe fn bitSet(in_0: u32, A: *mut u8) {
    let i = in_0.wrapping_rem((8 * (4 * 1024 * 1024)) as libc::c_uint) as libc::c_int;
    let j = i >> 3;
    let k = i - (j << 3);
    let ref mut fresh0 = *A.offset(j as isize);
    *fresh0 = (*fresh0 as libc::c_int | 1 << k) as u8;
}
#[no_mangle]
unsafe fn bitTest(in_0: u32, A: *mut u8) -> libc::c_int {
    let i = in_0.wrapping_rem((8 * (4 * 1024 * 1024)) as libc::c_uint) as libc::c_int;
    let j = i >> 3;
    let k = i - (j << 3);
    *A.offset(j as isize) as libc::c_int & 1 << k
}
#[no_mangle]
unsafe fn mapClear(A: *mut u8) {
    libc::memset(A as *mut libc::c_void, 0, (4 * 1024 * 1024) as usize);
}
#[no_mangle]
unsafe fn initMaps() {
    for i in 0..3 {
        if map[i as usize].is_null() {
            map[i as usize] = libc::malloc(4 * 1024 * 1024) as *mut u8
        }
        mapClear(map[i as usize]);
    }
}
#[no_mangle]
unsafe fn replaySet(configuration: &crate::config::Configuration, key: u32, time_0: libc::c_int) {
    let modKey: libc::c_int =
        key.wrapping_rem((8 * (4 * 1024 * 1024)) as libc::c_uint) as libc::c_int;
    let window: libc::c_int =
        (time_0 as libc::c_uint).wrapping_div(configuration.deltaT) as libc::c_int;
    for i in 0..3 {
        if mapWindow[i as usize] == window {
            bitSet(modKey as u32, map[i as usize]);
            return;
        }
    }
    for i in 0..3 {
        if mapWindow[i as usize] != window - 1
            && mapWindow[i as usize] != window
            && mapWindow[i as usize] != window + 1
        {
            mapWindow[i as usize] = window;
            mapClear(map[i as usize]);
            bitSet(modKey as u32, map[i as usize]);
            return;
        }
    }
}
/* **************************************************************/
/*  Cloaked Access Protocol Daemon                             */
/*  (C) 2012-2013 Aeolus Technologies, Inc.                    */
/*  All Rights Reserved.                                       */
/*                                                             */
/*  Build command: gcc -lrt -lcrypto -O3 -Wall -o capd capd.c  */
/* **************************************************************/
pub unsafe fn start_processes(mut configuration: config::Configuration) -> libc::c_int {
    /* Security Checks and File/Directory Setup */
    let pw: *mut libc::passwd;
    pw = getpwnam(
        std::ffi::CString::new(configuration.user.clone())
            .unwrap()
            .as_ptr(),
    );
    if pw.is_null() {
        crate::utility::fatal("SERVER HALT - User not found");
    }
    configuration.uid = (*pw).pw_uid as libc::c_int;
    configuration.gid = (*pw).pw_gid as libc::c_int;
    libc::mkdir(configuration.JAIL_PATH.as_ptr() as *const i8, 64);
    libc::chown(configuration.JAIL_PATH.as_ptr() as *const i8, 0, 0);
    libc::chmod(configuration.JAIL_PATH.as_ptr() as *const i8, 64);
    libc::chown(configuration.PASSWD_FILE.as_ptr() as *const i8, 0, 0);
    libc::chmod(configuration.PASSWD_FILE.as_ptr() as *const i8, 384);
    libc::chown(configuration.COUNTERFILE.as_ptr() as *const i8, 0, 0);
    libc::chmod(configuration.COUNTERFILE.as_ptr() as *const i8, 384);
    /* Fork into processes and establish sockets connections */
    let mut sockets: [libc::c_int; 2] = [0; 2];
    libc::socketpair(1, libc::SOCK_STREAM as libc::c_int, 0, sockets.as_mut_ptr());
    let netPID = libc::fork();
    if 0 == netPID {
        let netToCoreSocket = sockets[0];
        libc::close(sockets[1]);
        net::netProcess(configuration, netToCoreSocket);
        return 0;
    }
    let coreToNetSocket = sockets[1];
    libc::close(sockets[0]);
    libc::socketpair(1, libc::SOCK_STREAM as libc::c_int, 0, sockets.as_mut_ptr());
    let corePID = libc::fork();
    if 0 == corePID {
        let coreToAuthSocket = sockets[0];
        libc::close(sockets[1]);
        crate::core::coreProcess(configuration.clone(), coreToNetSocket, coreToAuthSocket);
        return 0;
    }
    libc::close(coreToNetSocket);
    let authToCoreSocket = sockets[1];
    libc::close(sockets[0]);
    auth::authProcess(configuration, authToCoreSocket, netPID, corePID);
    0
}

fn parse_args() -> config::Configuration {
    let matches = App::new("capd - Cloaked Access Protocol Daemon")
        .version("1.0")
        .about("Provides secure access")
        .arg(
            Arg::with_name("passwordfile")
                .short("f")
                .long("passwordfile")
                .value_name("PASSWORDFILE")
                .help("capd password file")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("counterfile")
                .short("c")
                .long("counterfile")
                .value_name("COUNTERFILE")
                .help("capd counter file")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("logfile")
                .short("l")
                .long("logfile")
                .value_name("LOGFILE")
                .help("capd log file")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("jaildir")
                .short("j")
                .long("jaildir")
                .value_name("JAILDIR")
                .help("capd jail directory")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("deltat")
                .short("dt")
                .long("deltat")
                .value_name("DELTA_T")
                .help("maximum packet timestamp variation in seconds")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("timeout")
                .short("t")
                .long("timeout")
                .value_name("TIME_LIMIT")
                .help("time limit for connection init in seconds")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("spoof-timeout")
                .short("q")
                .long("spoof-timeout")
                .value_name("SPOOF_TIMEOUT")
                .help("time limit for spoofed connection init")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("user")
                .short("u")
                .long("user")
                .value_name("USER")
                .help("user for drop privilege")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("script")
                .short("s")
                .long("script")
                .value_name("SCRIPT")
                .help("fully resolved path to firewall script")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("port")
                .short("p")
                .long("port")
                .value_name("PORT")
                .help("UDP port")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("address")
                .short("a")
                .long("address")
                .value_name("ADDRESS")
                .help("Server interface IP address")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("verbosity")
                .short("v")
                .long("verbosity")
                .value_name("VERBOSITY")
                .help("verbosity level"),
        )
        .get_matches();

    config::Configuration {
        COUNTERFILE: matches
            .value_of("counterfile")
            .unwrap_or("/etc/capd/capd")
            .to_string(),
        PASSWD_FILE: matches
            .value_of("passwordfile")
            .unwrap_or("/etc/capd/capd.passwd")
            .to_string(),
        LOG_FILE: matches
            .value_of("logfile")
            .unwrap_or("/var/log/capd")
            .to_string(),
        JAIL_PATH: matches
            .value_of("jaildir")
            .unwrap_or("/tmp/capd")
            .to_string(),
        OPEN_SSH_PATH: matches
            .value_of("script")
            .unwrap_or("/usr/sbin/openClose")
            .to_string(),
        SERVER_ADDRESS: matches.value_of("address").unwrap_or("").to_owned(),
        deltaT: matches
            .value_of("deltat")
            .unwrap_or("30")
            .parse::<u32>()
            .unwrap_or(30),
        initTimeout: matches
            .value_of("deltat")
            .unwrap_or("5")
            .parse::<u32>()
            .unwrap_or(5),
        spoofTimeout: matches
            .value_of("deltat")
            .unwrap_or("30")
            .parse::<u32>()
            .unwrap_or(30),
        user: matches.value_of("user").unwrap_or("capd").to_string(),
        uid: 0,
        gid: 0,
        port: matches
            .value_of("port")
            .unwrap_or("62201")
            .parse::<u16>()
            .unwrap_or(62201),
        verbosity: 1,
    }
}

fn main() {
    let configuration = parse_args();
    unsafe { ::std::process::exit(start_processes(configuration)) }
}
