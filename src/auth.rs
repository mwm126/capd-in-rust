use crate::to_u8;
use crate::IpAddr;

/* ***********************************/
/*   Authorization of connections   */
/* ***********************************/
#[no_mangle]
pub unsafe fn authProcess(
    configuration: crate::config::Configuration,
    authToCoreSocket: libc::c_int,
    netPID: libc::c_int,
    corePID: libc::c_int,
) {
    // let mut tmp: [libc::c_char; 15] = [0; 15];
    let mut hostAddr_buf: [libc::c_char; 32] = [0; 32];
    let mut allowedAddr_buf: [libc::c_char; 46] = [0; 46];
    let mut port_0: libc::c_int = 0;
    let mut timeLimit: libc::c_int = 0;
    /* This process retains privilege and is unjailed. */
    loop {
        /* Wait for authorization information */
        if libc::read(
            authToCoreSocket,
            allowedAddr_buf.as_mut_ptr() as *mut libc::c_void,
            ::std::mem::size_of::<[libc::c_char; 46]>(),
        ) as libc::c_ulong
            != ::std::mem::size_of::<[libc::c_char; 46]>() as libc::c_ulong
            || libc::read(
                authToCoreSocket,
                hostAddr_buf.as_mut_ptr() as *mut libc::c_void,
                ::std::mem::size_of::<[libc::c_char; 32]>(),
            ) as libc::c_ulong
                != ::std::mem::size_of::<[libc::c_char; 32]>() as libc::c_ulong
            || libc::read(
                authToCoreSocket,
                &mut port_0 as *mut libc::c_int as *mut libc::c_void,
                ::std::mem::size_of::<libc::c_int>(),
            ) as libc::c_ulong
                != ::std::mem::size_of::<libc::c_int>() as libc::c_ulong
            || libc::read(
                authToCoreSocket,
                &mut timeLimit as *mut libc::c_int as *mut libc::c_void,
                ::std::mem::size_of::<libc::c_int>(),
            ) as libc::c_ulong
                != ::std::mem::size_of::<libc::c_int>() as libc::c_ulong
        {
            /* Kill all capd processes if wrong-sized message arrives. */
            libc::kill(netPID, 9i32);
            libc::kill(corePID, 9i32);
            crate::utility::fatal(
                "CRITICAL FAULT - Wrong-sized message received from Auth Process!",
            );
        }

        let allowedAddr: IpAddr = std::ffi::CString::new(to_u8(&allowedAddr_buf))
            .unwrap()
            .into_string()
            .unwrap()
            .parse()
            .unwrap();
        let hostAddr: IpAddr = std::ffi::CString::new(to_u8(&hostAddr_buf))
            .unwrap()
            .into_string()
            .unwrap()
            .parse()
            .unwrap();

        /* Sanitize allowedAddr and hostAddr to prevent script hijacking */
        /* Place hard limits on int values */
        timeLimit = if 0i32
            > (if 300i32 < timeLimit {
                300i32
            } else {
                timeLimit
            }) {
            0i32
        } else if 300i32 < timeLimit {
            300i32
        } else {
            timeLimit
        };
        port_0 = if 0i32 > (if 65535i32 < port_0 { 65535i32 } else { port_0 }) {
            0i32
        } else if 65535i32 < port_0 {
            65535i32
        } else {
            port_0
        };
        /* Build command line for openSSH.sh call */
        let mut commandLine = format!(
            "{} {} {} {} {} &",
            configuration.OPEN_SSH_PATH, allowedAddr, hostAddr, port_0, timeLimit
        );
        /* Execute openSSH.sh command */
        libc::system(commandLine.as_mut_ptr() as *mut i8);
    }
}
