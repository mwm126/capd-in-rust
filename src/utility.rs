/* ********************************************/
/*             Utility Functions             */
/* ********************************************/
#[no_mangle]
pub fn currentTime() -> u32 {
    let mut time1 = libc::timespec {
        tv_sec: 0,
        tv_nsec: 0,
    };
    unsafe { crate::clock_gettime(0i32, &mut time1) };
    time1.tv_sec as u32
}
#[no_mangle]
pub fn fatal(msg: &str) {
    unsafe { libc::perror(std::ffi::CString::new(msg).unwrap().into_raw()) };
    std::process::exit(1);
}

pub struct Logger {
    configuration: crate::config::Configuration,
}

impl Logger {
    pub fn new(configuration: crate::config::Configuration) -> Logger {
        Logger { configuration }
    }

    #[no_mangle]
    pub fn logOutput(&self, f: *mut libc::FILE, l: libc::c_int, string: &str) {
        let mut timeStr: [libc::c_char; 100] = [0; 100];
        let mut rawtime: libc::time_t = 0;
        if self.configuration.verbosity < l {
            return;
        }
        unsafe {
            libc::time(&mut rawtime);
            let info = libc::localtime(&mut rawtime);
            crate::strftime(
                timeStr.as_mut_ptr(),
                100,
                b"%X %x\x00" as *const u8 as *const libc::c_char,
                info,
            );
            libc::fprintf(
                f,
                b"[%s] \x00" as *const u8 as *const libc::c_char,
                timeStr.as_mut_ptr(),
            );
            print!("{}", string);
            libc::fprintf(f, b"\n\x00" as *const u8 as *const libc::c_char);
            libc::fflush(f);
        }
    }
}

pub fn arrayToHexString(mut _binary: &[u8]) -> String {
    format!("{:x?}", _binary)
}
